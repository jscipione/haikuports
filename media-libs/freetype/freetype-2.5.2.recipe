SUMMARY="A Free, High-Quality, and Portable Font Engine"
DESCRIPTION="
FreeType is written in C, designed to be small, efficient, highly \
customizable, and portable while capable of producing high-quality output \
(glyph images) of most vector and bitmap font formats.
"
HOMEPAGE="http://www.freetype.org"
LICENSE="FreeType"
COPYRIGHT="1996-2013 David Turner, Robert Wilhelm, Werner Lemberg, et al."
SRC_URI="http://download.savannah.gnu.org/releases/freetype/freetype-2.5.2.tar.bz2"
CHECKSUM_SHA256="4ff4bd393aa01071ec7b849d035508a505a78f88b2bcf25ff11e58e43c0b9def"
REVISION="3"
ARCHITECTURES="x86_gcc2 x86 x86_64 arm"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	freetype$secondaryArchSuffix = $portVersion
	lib:libfreetype$secondaryArchSuffix = 6.11.1 compat >= 6
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libbz2$secondaryArchSuffix
	lib:libz$secondaryArchSuffix >= 1
	lib:libpng$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libbz2$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libpng$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtool >= 2.4.2
	cmd:make
	cmd:which
	"

BUILD()
{
	./autogen.sh
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libfreetype
	fixPkgconfig

	# Freetype headers are in a freetype2 subdirectory, but fixPkgconfig removes
	# that from the pkg-config file, making it difficult for other ports to use
	# freetype with pkg-config.
	sed -i -e "s,^includedir=.*,includedir=$prefix/$relativeIncludeDir/freetype2," \
   		$developLibDir/pkgconfig/freetype2.pc

	# devel package
	packageEntries devel \
		$binDir \
		$dataRootDir/aclocal \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	freetype${secondaryArchSuffix}_devel = $portVersion
	cmd:freetype_config${secondaryArchSuffix} = $portVersion
	devel:libfreetype${secondaryArchSuffix} = 6.11.1 compat >= 6
	"
REQUIRES_devel="
	freetype${secondaryArchSuffix} == $portVersion base
	"
