SUMMARY="C library for reading and writing files containing sampled sound"
DESCRIPTION="
libsndfile is a library of C routines for reading and writing \
files containing sampled audio data.
"
HOMEPAGE="http://www.mega-nerd.com/libsndfile"
COPYRIGHT="
	1999-2011 Erik de Castro Lopo
"
LICENSE="GNU LGPL v2.1"
SRC_URI="http://www.mega-nerd.com/libsndfile/files/$portVersionedName.tar.gz"
CHECKSUM_SHA256="59016dbd326abe7e2366ded5c344c853829bebfd1702ef26a07ef662d6aa4882"
REVISION="1"
ARCHITECTURES="!x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="!x86_gcc2 x86"

PATCHES="$portVersionedName.patchset"

PROVIDES="
	libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	cmd:sndfile_cmp
	cmd:sndfile_concat
	cmd:sndfile_convert
	cmd:sndfile_deinterleave
	cmd:sndfile_info
	cmd:sndfile_interleave
	cmd:sndfile_metadata_get
	cmd:sndfile_metadata_set
	cmd:sndfile_play
	cmd:sndfile_regtest
	cmd:sndfile_salvage
	lib:libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libflac$secondaryArchSuffix
	lib:libogg$secondaryArchSuffix
	lib:libvorbis$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libflac$secondaryArchSuffix
	devel:libogg$secondaryArchSuffix
	devel:libvorbis$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:pkg_config
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal -I M4
	autoconf
	automake
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libsndfile
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libsndfile${secondaryArchSuffix}_devel = $portVersion compat >= 1
	devel:libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	"
REQUIRES_devel="
	libsndfile$secondaryArchSuffix == $portVersion base
	"
