SUMMARY="A library that defines common error values"
DESCRIPTION="
This is a library that defines common error values for all GnuPG components. \
Among these are GPG, GPGSM, GPGME, GPG-Agent, libgcrypt, Libksba, DirMngr, \
Pinentry, SmartCard Daemon and more.
"
LICENSE="GNU LGPL v2.1"
COPYRIGHT="2003-2013 g10 Code GmbH"
HOMEPAGE="http://www.gnupg.org/related_software/libraries.en.html#lib-libgpg-error"
SRC_URI="ftp://ftp.gnupg.org/gcrypt/libgpg-error/libgpg-error-1.12.tar.bz2"
CHECKSUM_SHA256="cafc9ed6a87c53a35175d5a1220a96ca386696eef2fa059cc0306211f246e55f"
REVISION="2"
ARCHITECTURES="x86"

PROVIDES="
	libgpg_error = $portVersion compat >= 1
	lib:libgpg_error = 0.10.0 compat >= 0
	cmd:gpg_error = $portVersion compat >= 1
	"

REQUIRES="
	haiku >= $haikuVersion
	"

BUILD_REQUIRES="
	haiku_devel >= $haikuVersion
	"

BUILD_PREREQUIRES="
	cmd:awk
	cmd:make
	cmd:gcc
	"

SOURCE_DIR="libgpg-error-$portVersion"

PATCHES="libgpg_error-$portVersion.patch"

BUILD()
{
	runConfigure ./configure
	make
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libgpg-error

	packageEntries devel \
		$developDir $binDir/gpg-error-config
}

PROVIDES_devel="
	libgpg_error_devel = $portVersion compat >= 1
	cmd:gpg_error_config = $portVersion compat >= 1
	devel:libgpg_error = 0.10.0 compat >= 0
	"

REQUIRES_devel="
	libgpg_error == $portVersion
	"
