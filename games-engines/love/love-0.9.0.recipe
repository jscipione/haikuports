SUMMARY="LÖVE is an *awesome* framework you can use to make 2D games in Lua."
DESCRIPTION="
Hi there! LÖVE is an *awesome* framework you can use to make 2D games in Lua.
It's free, open-source, and works on Windows, Mac OS X and Linux.
"
HOMEPAGE="http://love2d.org/"
SRC_URI="https://bitbucket.org/rude/love/downloads/love-0.9.0-linux-src.tar.gz"
CHECKSUM_SHA256="4f0d24945223fc3ba78f0a81799a179f06b97e9b0aea2c9c6ad532820a491611"
REVISION="1"
LICENSE="Zlib"
COPYRIGHT="2010-2014 Löve"

ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	love$secondaryArchSuffix = $portVersion
	cmd:love$secondaryArchSuffix = $portVersion
	lib:liblove$secondaryArchSuffix = $portVersion
"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libSDL2_2.0$secondaryArchSuffix
	lib:libvorbis$secondaryArchSuffix
	lib:libvorbisfile$secondaryArchSuffix
	lib:libmodplug$secondaryArchSuffix >= 0.8.0
	lib:libphysfs$secondaryArchSuffix
	lib:libluajit_5.1$secondaryArchSuffix
	lib:libopenal$secondaryArchSuffix
	lib:libogg$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	lib:libIL$secondaryArchSuffix
	lib:libGL$secondaryArchSuffix
	lib:libmpg123$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libSDL2$secondaryArchSuffix
	devel:libvorbis$secondaryArchSuffix
	devel:libvorbisfile$secondaryArchSuffix
	devel:libmodplug$secondaryArchSuffix >= 0.8.0
	devel:libphysfs$secondaryArchSuffix
	devel:libluajit_5.1$secondaryArchSuffix
	devel:libopenal$secondaryArchSuffix
	devel:libogg$secondaryArchSuffix
	devel:libfreetype$secondaryArchSuffix
	devel:libIL$secondaryArchSuffix
	devel:libGL$secondaryArchSuffix
	devel:libmpg123$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
"
BUILD_PREREQUIRES="
	cmd:make
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:pkg_config$secondaryArchSuffix
	cmd:awk
"

PATCHES="love-0.9.0.patchset"
BUILD()
{
	runConfigure ./configure
	# The pkg-config for freetype doesn't give this directory, which means
	# there is no good way of finding it...
	make $jobArgs CXXFLAGS="-I/system/develop/headers/x86/freetype2/ --std=c++11"
}

INSTALL()
{
	make install

	prepareInstalledDevelLib liblove
	packageEntries devel $developDir
}

PROVIDES_devel="
	love${secondaryArchSuffix}_devel = $portVersion
	devel:liblove$secondaryArchSuffix = $portVersion
"

REQUIRES_devel="
	love$secondaryArchSuffix == $portVersion base
"
