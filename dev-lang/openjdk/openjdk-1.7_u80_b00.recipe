SUMMARY="Open-source implementation of the Java Platform, SE"
DESCRIPTION="
Open-source implementation of the Java Platform, Standard Edition.
"
HOMEPAGE="http://openjdk.java.net/"
COPYRIGHT="2005, 2006, Oracle and/or its affiliates"
LICENSE="GNU GPL v2"

SRC_URI="https://dl.dropboxusercontent.com/u/61946213/j2sdk-image-140528.tar.xz"
CHECKSUM_SHA256="fe9ad5156baf62a14213667f18bf88035daf5f24d6370afc9af30d079d8609a3"
SOURCE_DIR="j2sdk-image"

SRC_URI_2="http://www.eu.apache.org/dist/ant/binaries/apache-ant-1.9.4-bin.zip"
CHECKSUM_SHA256_2="973d97d656e65561e2753823de6cf6db35570aaf93eeec6e7cb3d68e1ec4d0e8"
SOURCE_DIR_2="apache-ant-1.9.4"

SRC_URI_3="hg+http://hg.openjdk.java.net/jdk7u/jdk7u#jdk7u80-b00"
SRC_URI_4="hg+http://hg.openjdk.java.net/jdk7u/jdk7u/langtools#jdk7u80-b00"
SRC_URI_5="hg+http://bitbucket.org/hamishm/haiku-jdk7u-corba"
SRC_URI_6="hg+http://hg.openjdk.java.net/jdk7u/jdk7u/jaxp#jdk7u80-b00"
SRC_URI_7="hg+http://hg.openjdk.java.net/jdk7u/jdk7u/jaxws#jdk7u80-b00"
SRC_URI_8="hg+http://bitbucket.org/hamishm/haiku-jdk7u-hotspot"
SRC_URI_9="hg+http://bitbucket.org/hamishm/haiku-jdk7u-jdk"

REVISION="2"
ARCHITECTURES="x86"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

DISABLE_SOURCE_PACKAGE=yes
	# at least as long as Ant and a complete SDK image are part of the sources

PROVIDES="
	openjdk = $portVersion compat >= 1
	cmd:apt = $portVersion compat >= 1
	cmd:extcheck = $portVersion compat >= 1
	cmd:idlj = $portVersion compat >= 1
	cmd:jar = $portVersion compat >= 1
	cmd:jarsigner = $portVersion compat >= 1
	cmd:java = $portVersion compat >= 1
	cmd:java_config = $portVersion compat >= 1
	cmd:javac = $portVersion compat >= 1
	cmd:javadoc = $portVersion compat >= 1
	cmd:javah = $portVersion compat >= 1
	cmd:javap = $portVersion compat >= 1
	cmd:jcmd = $portVersion compat >= 1
	cmd:jconsole = $portVersion compat >= 1
	cmd:jdb = $portVersion compat >= 1
	cmd:jhat = $portVersion compat >= 1
	cmd:jinfo = $portVersion compat >= 1
	cmd:jmap = $portVersion compat >= 1
	cmd:jps = $portVersion compat >= 1
	cmd:jrunscript = $portVersion compat >= 1
	cmd:jsadebugd = $portVersion compat >= 1
	cmd:jstack = $portVersion compat >= 1
	cmd:jstat = $portVersion compat >= 1
	cmd:jstatd = $portVersion compat >= 1
	cmd:keytool = $portVersion compat >= 1
	cmd:native2ascii = $portVersion compat >= 1
	cmd:orbd = $portVersion compat >= 1
	cmd:pack200 = $portVersion compat >= 1
	cmd:rmic = $portVersion compat >= 1
	cmd:rmid = $portVersion compat >= 1
	cmd:rmiregistry = $portVersion compat >= 1
	cmd:schemagen = $portVersion compat >= 1
	cmd:serialver = $portVersion compat >= 1
	cmd:servertool = $portVersion compat >= 1
	cmd:tnameserv = $portVersion compat >= 1
	cmd:unpack200 = $portVersion compat >= 1
	cmd:wsgen = $portVersion compat >= 1
	cmd:wsimport = $portVersion compat >= 1
	cmd:xjc = $portVersion compat >= 1
	"

REQUIRES="
	haiku$secondaryArchSuffx >= $haikuVersion
	lib:libfreetype$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libfreetype$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:cpio >= 2.10
	cmd:make
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:sed
	cmd:tar
	cmd:zip
	cmd:awk
	cmd:hostname
	cmd:find
	cmd:unzip
	cmd:unzipsfx
	cmd:head
	"

BUILD()
{
	OPENJDK_DIR=$(dirname $sourceDir)

	# prepare the directory structure
	cd $OPENJDK_DIR
	ln -sfn $sourceDir3 jdk
	ln -sfn $sourceDir4 jdk/langtools
	ln -sfn $sourceDir5 jdk/corba
	ln -sfn $sourceDir6 jdk/jaxp
	ln -sfn $sourceDir7 jdk/jaxws
	ln -sfn $sourceDir8 jdk/hotspot
	ln -sfn $sourceDir9 jdk/jdk

	chmod u+x ${sourceDir2}/bin/ant

	# set up environment
	export ALT_BOOTDIR=${OPENJDK_DIR}/j2sdk-image
	export ALT_JDK_IMPORT_PATH=$ALT_BOOTDIR
	export ANT_HOME=${sourceDir2}
	export ALT_OUTPUTDIR=${OPENJDK_DIR}/output
	export ALT_UNIXCOMMAND_PATH=

	export LC_ALL=C

	# If ASLR is enabled, the JVM can fail to find a large enough area for
	# the heap.
	export DISABLE_ASLR=1

	if [ -n "$secondaryArchSuffix" ]; then
		export ALT_COMPILER_PATH=`finddir B_SYSTEM_BIN_DIRECTORY`$secondaryArchSubDir
		export ALT_FREETYPE_HEADERS_PATH=`finddir B_SYSTEM_HEADERS_DIRECTORY`$secondaryArchSubDir
	fi

	export USE_EXTERNAL_ZLIB=true

	# Build.
	cd jdk
	make
}

INSTALL()
{
	# install the generated SDK image dir
	cd ..
	jdkDir=$libDir/openjdk
	mkdir -p $(dirname $jdkDir)
	cp -a output/j2sdk-image $jdkDir

	# symlink the executables to binDir
	mkdir -p $binDir
	symlinkRelative -s $jdkDir/bin/* $binDir
		# TODO: We should probably link only a subset.

	# create a java-config script in binDir
	javaConfig=$binDir/java-config
	sed -e "s,%JAVA_HOME%,$jdkDir," $portDir/sources/java-config.in \
		> $javaConfig
	chmod a+x $javaConfig

	# create a profile.d file that sets up JAVA_HOME
	jdkProfile=$dataDir/profile.d/openjdk.sh
	mkdir -p $(dirname $jdkProfile)
	echo 'JAVA_HOME=`java-config -H`' > $jdkProfile
	echo "export JAVA_HOME" >> $jdkProfile
}
